package com.nz.alexbutenko.schoolbirthdays;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;

/**
 * Created by alexbutenko on 8/13/17.
 */

public class BirthdaysFirebaseInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = "FCM Service";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        try {
            NetworkUtils.addDevice(android.os.Build.MODEL, token);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}