package com.nz.alexbutenko.schoolbirthdays;

import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public class NetworkUtils {

    private static final String BIRTHDAYS_BASE_URL = "https://rocky-lowlands-87135.herokuapp.com/";
    private static final String TAG = "NetworkUtils";

    public static class Child {
        public final int id;
        public final String name;
        public final String grade;
        public final String bday;

        public Child(int id, String name, String grade, String bday) {
            this.id = id;
            this.name = name;
            this.grade = grade;
            this.bday = bday;
        }
    }

    public static class Device {
        public final String title;
        public final String token;

        public Device(String title, String token) {
            this.title = title;
            this.token = token;
        }
    }

    public static Retrofit getRetrofit() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BIRTHDAYS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit;
    }

    public static String[] getBirthdays() throws IOException {
        BirthdayService service = getRetrofit().create(BirthdayService.class);
        Call<List<Child>> birthdaysResponse = service.listBirthdays();
        List<Child> children = birthdaysResponse.execute().body();

        String[] result = new String[children.size()];

        for (int i = 0; i < children.size(); i++) {
            Child child = children.get(i);

            result[i] = child.name + " - " + child.bday;
        }

        return result;
    }

    public static void addDevice(String title, String token) throws IOException {
        DeviceService service = getRetrofit().create(DeviceService.class);

        Log.i(TAG, "creating: " +  title + "   " +  token);
        Call<Device> deviceResponse = service.createDevice(new Device(title, token));
        Device result = deviceResponse.execute().body();
        Log.i(TAG, "result: " + result.title + "   " + result.token);
    }

    public interface BirthdayService {
        @GET("recent_birthdays")
        Call<List<Child>> listBirthdays();
    }

    public interface DeviceService {
        @POST("devices")
        Call<Device> createDevice(@Body Device device);
    }
}


