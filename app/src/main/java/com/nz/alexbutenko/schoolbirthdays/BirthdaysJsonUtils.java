package com.nz.alexbutenko.schoolbirthdays;

/**
 * Created by alexbutenko on 7/30/17.
 */

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BirthdaysJsonUtils {
    public static String[] getSimpleBirthdayStringsFromJson(String jsonString) throws JSONException {
        final String NAME_TAG = "name";
        final String BIRTHDAY_TAG = "bday";

        String[] parsedBirthdayData = null;

        try {
            // jsonString is a string variable that holds the JSON
            JSONArray birthdayArray = new JSONArray(jsonString);
            parsedBirthdayData = new String[birthdayArray.length()];

            for (int i = 0; i < birthdayArray.length(); i++) {
                JSONObject childInfo = birthdayArray.getJSONObject(i);
                String name = childInfo.getString(NAME_TAG);
                String birthdayString = childInfo.getString(BIRTHDAY_TAG);

                parsedBirthdayData[i] = name + " - " + birthdayString;
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return parsedBirthdayData;
    }
}
