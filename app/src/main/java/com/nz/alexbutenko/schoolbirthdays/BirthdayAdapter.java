package com.nz.alexbutenko.schoolbirthdays;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by alexbutenko on 7/30/17.
 */

public class BirthdayAdapter extends RecyclerView.Adapter<BirthdayAdapter.BirthdayAdapterViewHolder> {

    private String[] mBirthdayData;

    public class BirthdayAdapterViewHolder extends RecyclerView.ViewHolder {

        private TextView mBirthdayDataView;

        public BirthdayAdapterViewHolder(View view) {
            super(view);

            mBirthdayDataView = (TextView) view.findViewById(R.id.tv_birthday_data);
        }
    }

    @Override
    public BirthdayAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.birthday_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
        return new BirthdayAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BirthdayAdapterViewHolder holder, int position) {
        holder.mBirthdayDataView.setText(mBirthdayData[position]);
    }

    @Override
    public int getItemCount() {
        return mBirthdayData == null ? 0 : mBirthdayData.length;
    }

    public void setBirthdayData(String[] birthdayData) {
        mBirthdayData = birthdayData;
        notifyDataSetChanged();
    }
}
